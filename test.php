<?php
  if (isset($_GET['testID'])) {
    $testID = $_GET['testID'];
    $testPath = __DIR__ . '/tests/test-' . $testID . '.json';
    if (!file_exists($testPath)) {
      header("HTTP/1.0 404 Not Found");
      die();
    }
    $testContent = file_get_contents($testPath) or exit('Не удалось получить данные');
    $test = json_decode($testContent, true);
    if ($test === null) {exit('Ошибка декодирования JSON');}
    $testQuestions = $test['questions'];
  }

  if (!empty($_POST)) {
    $testResult = [];
    $testName = $test['name'];
    $testResult['testName'] = $testName;
    $testResult['userName'] = $_POST['user_name'];
    $testResult['userAnswers'] = [];
    $correctlyCount = 0;
    foreach($testQuestions as $questionIndex => $question) {
      $correctly = $question['correctly'];
      if (!array_key_exists("q$questionIndex", $_POST)) {
        $testResult['userAnswers'][$questionIndex]['userAnswer'] = NULL;
      }
      elseif ($correctly == $_POST['q'.$questionIndex]) {
        $testResult['userAnswers'][$questionIndex]['userAnswer'] = true;
        $correctlyCount++;
      }
      elseif ($correctly != $_POST['q'.$questionIndex]) {
        $testResult['userAnswers'][$questionIndex]['userAnswer'] = false;
        $testResult['userAnswers'][$questionIndex]['answerCorrect'] = $question['answers'][$correctly]['value'];
      }
    }
    $testResult['totalCorrect'] = $correctlyCount;
    $testResult['totalQuestion'] = count($testQuestions);

    session_start();
    $_SESSION['testResult'] = $testResult;
    header('Location: cert.php', true, 301);
  }
?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h1>Тестрирование</h1>
  <a href="admin.php">Добавить тест</a>
  <?php
    require_once('list.php');
  ?>
  <?php if (isset($_GET['testID'])) :?>
  <h2>Проходжение теста</h2>
    <form action="?testID=<?php echo $_GET['testID']?>" method="POST">
    <label for="user-name">Имя пользователя</label>
    <input type="text" name="user_name" id="user-name" required>
    <?php foreach ($test['questions'] as $questionCounter => $testItem) :?>
      <fieldset>
        <legend><?php echo (!empty($testItem['question']) ? $testItem['question'] : 'Не удалось получить вопрос') ?></legend>
        <?php for ($i = 0; $i < count($testItem['answers']); $i++) :?>
          <label>
            <input type="radio" name="<?php echo 'q'. $questionCounter ?>" 
            value="<?php echo $i ?>">
            <?php echo (!empty($testItem['answers'][$i]['value'])) ? $testItem['answers'][$i]['value'] : 'Не удалось получить вариант ответа' ?>
          </label>
        <?php endfor ?>
      </fieldset>
    <?php endforeach ?>
      <input type="submit" value="Отправить">  
    </form>
  <?php endif ?>
</body>
</html>
